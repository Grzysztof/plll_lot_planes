﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Globalization;

namespace plll_lot_planes
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        //tworzymy nową liste dla obiektów Plane i przypisujemy do niej wartości zwracane przez CallApi();
        IList<Plane> aircraftList = Plane_finder.CallApi();


        public MainWindow()
        {
            InitializeComponent();
            //Obsługa ListView - Dodajemy obiekty z aircraftList do widoku listView interfejsu.
            foreach (Plane plane in aircraftList)
            {
                ListView_UI.Items.Add(plane);
            }
            regBox.Text = "Aby wyświetlić więcej danych, wybierz samolot z listy.";

        }

        public void DisplayMore(int index) {
            //z powodu problemów z formatowaniem tworzymy nowy format który przy konwersji np. z float na string uzywa kropki.
            NumberFormatInfo dot = new NumberFormatInfo();
            dot.NumberDecimalSeparator = ".";
            //konwertujemy długość i szerokośc geograficzna do typu string
            string plane_lat = aircraftList[index].Lat.ToString(dot);
            string plane_long = aircraftList[index].Long.ToString(dot);
            //Kożystając z serwisu staticmap.openstreetmap.de pobieramy obraz .png z interesującym nas obszarem i zaznaczoną pozycja samolotu.
            string mapUrl = "http://staticmap.openstreetmap.de/staticmap.php?center=" + plane_lat + "," + plane_long + "&zoom=6&size=580x360&maptype=mapnik&markers=" + plane_lat + "," + plane_long + ",lightBlue1";
            //ładujemy obrazek i dodajemy go do interfejsu
            mapImg.Source = new BitmapImage(new Uri(mapUrl));
            //przypisujemy dane
            regBox.Text = "Numer rejestracyjny: " + aircraftList[index].Reg;
            mdlBox.Text = "Model samolotu: " + aircraftList[index].Mdl;
            altBox.Text = "Aktualna wysokość: " + aircraftList[index].Alt.ToString(dot) + " ft";
            latBox.Text = "Latitude: " + aircraftList[index].Lat.ToString(dot);
            longBox.Text = "Longitude: " + aircraftList[index].Long.ToString(dot);
            calBox.Text = "Znak wywoławczy (Callsign): " + aircraftList[index].Call;
            fromBox.Text = "Z: " + aircraftList[index].From;
            toBox.Text = "Do: " + aircraftList[index].To;
            posBox.Text = "Pozycja samolotu na mapie:";
        }
        
        //funkcja obsługujące kliknięcie w wiersz listy listView
        private void ListView_UI_Selected(object sender, RoutedEventArgs e)
        {
            DisplayMore(ListView_UI.SelectedIndex);
        }

    }
}
