﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
/*Projekt zaliczeniowy Krzysztof Myjak 09.02.2018
 * Celem programu jest wyśiwtlenie wszystkich znajdujących
 * sie aktualnie w powietrzy samolotów linii lotniczych LOT */


namespace plll_lot_planes
{
    //definiujemy naszego obiektu samolot i jakie własciwosci pobieramy z api.
    class Plane
    {

        public string Reg { get; set; } //rejestracja samolotu
        public float Alt { get; set; } //wysokosc
        public float Lat { get; set; } //szerokosc geograficzna
        public float Long { get; set; }// długośc geograficzna
        public String Type { get; set; } //typ samolotu
        public String Mdl { get; set; } // pełna nazwa modelu
        public String Call { get; set; } //Znak wywoławczy
        public String Op { get; set; }  //Skrótowa nazwa operatora/wlasciciela samolotu
        public String From { get; set; } // lotnisko startu
        public String To { get; set; } //lotnisko ladowania
    }

     static class Plane_finder { 
            //funkcja pobierająca dane w formacie JSON ze strony adsbexchange.
            public static IList<Plane> CallApi()
            {
                string url = "https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?fOpIcaoQ=LOT";
                //Tworzymy nowe zapytanie do serwera Api o podanym adresie 
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                try
                {
                    //pobieramy odpowiedz
                    WebResponse response = request.GetResponse();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        //parsujemy odpowiedz z formatu JSON do naszych obiektów
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        string jsonResponseText = reader.ReadToEnd();
                        JObject jsonResponse = JObject.Parse(jsonResponseText);
                        try
                        {
                            //wybieramy dane które nas interesują, w tym przypadku wszystko znajduje sie w obiekcie JSON o nazwie acList
                            IList<JToken> results = jsonResponse["acList"].Children().ToList();
                            
                            //tworzymy nową listę gotową na przyjecie naszych obiektów Plane
                            IList<Plane> planes = new List<Plane>();
                            //dodajemy kolejne obiekty z Pliku JSON do naszej nowej listy wraz z wartosciami które nas interesują 
                            foreach (JToken result in results)
                            {
                                Plane plane = result.ToObject<Plane>();
                                planes.Add(plane);
                            }
                            return planes; //zwracamy Liste
                        }
                        catch
                        {
                            Console.WriteLine("ERR");
                        }
                        return null;
                    }
                }
                catch (WebException ex)
                {
                    WebResponse errorResponse = ex.Response;
                    using (Stream responseStream = errorResponse.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                        String errorText = reader.ReadToEnd();
                        // log errorText
                    }
                    throw;
                }
            }

        }
}